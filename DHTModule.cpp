#include "DHTModule.h"

/**
 * Returns combined value of integer side and floating point side.
 * @param byte data0 Integer side value.
 * @param byte data1 Floating point side value.
 * @return float Combined value of integer side and floating point side.
 */
float DHTModule::combineData(byte data0, byte data1) {
	float value = data1;
	bool criticalElse = true;
	byte criticals[] = {10, 100};
	const byte LENGTH = sizeof(criticals) / sizeof(byte);
	for (byte i = 0; i < LENGTH; i++) {
		if (value < criticals[i]) {
			criticalElse = false;
			value /= criticals[i];
			break;
		}
	}
	if (criticalElse) {
		value /= 1000;
	}
	return value + data0;
}

/**
 * Waiting the DHT digital signal until to be a target digital signal.
 * @param byte v The level of digital signal.
 */
void DHTModule::wait(byte signal) {
	unsigned int retry = DHTModule::MAX_RETRY;
	while (digitalRead(this->sigPin) == signal) {
		if (retry-- == 0) {
			return;
		}
	}
}

/**
 * Returns the read signal value.
 * @return byte The read signal value.
 */
byte DHTModule::read() {
	byte data = 0;
	for (byte i = 0x80; i > 0; i >>= 1) {
		this->wait(LOW);
		unsigned long microseconds = micros();
		this->wait(HIGH);
		if ((micros() - microseconds) > 40) {
			data |= i;
		}
	}
	return data;
}

/**
 * Returns the Fahrenheit convert from Celsius.
 * @param float c The Celsius will convert to Fahrenheit.
 * @return float The Fahrenheit convert from Celsius.
 */
float DHTModule::CtoF(float c) {
	return c * 1.8 + 32;
}

/**
 * Constructs the DHTModule object passes the ioPin.
 * @param byte sigPin The pin will be written and read from MCU.
 */
DHTModule::DHTModule(byte sigPin) {
	this->sigPin = sigPin;
}

/**
 * Updates the humidity, temperature and checksum values from the sensor.
 */
void DHTModule::update() {
	// DHT cannot too fast in a cycle, otherwise the sensor will be jammed.
	pinMode(this->sigPin, OUTPUT);
	digitalWrite(this->sigPin, LOW);
	delay(20);
	digitalWrite(this->sigPin, HIGH);
	delayMicroseconds(40);
	pinMode(this->sigPin, INPUT);
	this->wait(LOW);
	this->wait(HIGH);
	const byte LENGTH = sizeof(this->data) / sizeof(byte);
	// data[0] is humidity of integer side
	// data[1] is humidity of floating point side
	// data[2] is terperature of integer side
	// data[3] is terperature of floating point side
	// data[4] is checksum which data[0] + data[1] + data[2] + data[3] == data[4]
	for (byte i = 0; i < LENGTH; i++) {
		this->data[i] = this->read();
	}
	delay(2000);
}

/**
 * Returns the Humidity value.
 * @return float The humidity value.
 */
float DHTModule::getHumidity() {
	return DHTModule::combineData(this->data[0], this->data[1]);
}

/**
 * Returns the Temperature value.
 * @return float The Temperature value.
 */
float DHTModule::getTemperature() {
	return DHTModule::combineData(this->data[2], this->data[3]);
}

/**
 * Checks if the checksum value is equal to the humidity and temperature.
 * @return bool True if checksum value is equal to the humidity and temperature.
 */
bool DHTModule::checksum() {
	unsigned int sum = 0;
	sum += this->data[0];
	sum += this->data[1];
	sum += this->data[2];
	sum += this->data[3];
	return (sum & 0xFF) == this->data[4];
}