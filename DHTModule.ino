#include "DHTModule.h"

byte VCC_PIN = 4;
byte SIG_PIN = 3;
byte GND_PIN = 2;
DHTModule dhtModule = DHTModule(SIG_PIN);

void setup() {
	Serial.begin(115200);
	pinMode(VCC_PIN, OUTPUT);
	digitalWrite(VCC_PIN, HIGH);
	pinMode(GND_PIN, OUTPUT);
	digitalWrite(GND_PIN, LOW);
}

void loop() {
	printDHTModule(dhtModule);
}

void printDHTModule(DHTModule dhtModule) {
	dhtModule.update();
	bool checksum = dhtModule.checksum();
	if (checksum) {
		float temperature = dhtModule.getTemperature();
		Serial.println();
		Serial.println(checksum);
		Serial.print("Humidity (%): ");
		Serial.println(dhtModule.getHumidity());
		Serial.print("Temperature (C): ");
		Serial.println(temperature);
		Serial.print("Temperature (F): ");
		Serial.println(DHTModule::CtoF(temperature));
	}
}