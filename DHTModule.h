#ifndef DHTMODULE_H
#define DHTMODULE_H

#include <Arduino.h>

class DHTModule {
	private:
		const static unsigned int MAX_RETRY = 65535;
		static float combineData(byte, byte);
		byte sigPin;
		byte data[5];
		void wait(byte);
		byte read();
	public:
		static float CtoF(float);
		DHTModule(byte);
		void update();
		float getHumidity();
		float getTemperature();
		bool checksum();
};

#endif